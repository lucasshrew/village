
import abc

class Indexes:
    GEO = 1

class IDBClient(metaclass=abc.ABCMeta):
    """Specifies a db client interface"""

    @abc.abstractmethod
    def connect(self, name='db_index'):
        return True

    @abc.abstractmethod
    def get_collection(self, name):
        """Gets or creates a collection if it doesn't exist"""
        return
    
    def drop_database(self, name):
        return


class IDBCollection(metaclass=abc.ABCMeta):
    """Specifies a db collection interface"""

    @abc.abstractmethod
    def create_index(self, key, type):
        return

    @abc.abstractmethod
    def insert(self, object):
        return None

    @abc.abstractmethod
    def get_all(self):
        return []

    @abc.abstractmethod
    def get(self, uid):
        return None

    @abc.abstractmethod
    def get_near(self, center, distance=20):
        return []

    def query(self, expresion):
        return []

