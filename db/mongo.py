
from bson.objectid import ObjectId
from pymongo import MongoClient, GEO2D, GEOSPHERE

import db

IndexesMap = {
        db.Indexes.GEO: GEOSPHERE,
    }

class Client(db.IDBClient):
    """A mongodb based db"""
        
    def __init__(self, args=None):
        super(Client, self).__init__()
        self._client = MongoClient(args)
        self._connection = None 
        self._collections = dict()
        
    def connect(self, name='db_index'):
        self._connection = self._client[name]
        return self._connection

    def get_collection(self, name):
        if name not in self._collections:
            collection = Collection(self._connection, name)
        return self._collections.setdefault(name, collection)

    def drop_database(self, name):
        self._client.drop_database(name)


class Collection(db.IDBCollection):
    """A mongodb based collection of documents"""

    def __init__(self, db, name):
        super(Collection, self).__init__()
        self._name = name
        self._db = db
        self._collection = db[name]
        self._indexes = {}

    def get(self, uid):
        return self._collection.find_one({'_id': ObjectId(uid)})

    def get_all(self):
        return self._collection.find()

    def get_near(self, center, distance=500):
        key = self._indexes[db.Indexes.GEO]
        
        ## TODO: we keep thes query if we decide to use GeoJSON
        #location = { "type": "Point", "coordinates": center }
        #query = {}
        #query["$near"] = {"$geometry": location}
        #query["$maxDistance"] = distance
        
        query = {"$within": {"$center": [center, distance]}}
        return self._collection.find({key: query})

    def insert(self, obj):
        return self._collection.insert(obj) 

    def create_index(self, key, indextype):
        cls = IndexesMap[indextype]
        self._collection.create_index([(key, cls)])
        self._indexes[indextype] = key

    def query(self, expression):
        return self._collection.find(expression)
