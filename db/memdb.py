
import db
from . import quadtree

IndexesMap = {
		db.Indexes.GEO: quadtree.QuadTree,
	}

class Client(db.IDBClient):
	"""A memory based db which index using quadtree"""
	
	def __init__(self):
		super(Client, self).__init__()
		self._connection = None
		self._collections = dict()
		
	def connect(self, name='db_index'):
		self._connection = name
		return True

	def get_collection(self, name):
		if name not in self._collections:
			collection = Collection(name)
		return self._collections.setdefault(name, collection)


class Collection(db.IDBCollection):
	"""A memory based collection of documents"""

	def __init__(self, name):
		super(Collection, self).__init__()
		self._name = name
		self._objects = dict()
		self._indexes = dict()
		self._current_id = 0

	def get(self, uid):
		return self._objects.get(uid, None)

	def get_all(self):
		return self._objects.values()

	def get_near(self, center, distance=20.0):
		index = self._indexes[db.Indexes.GEO]
		return index.get_near_points(center, distance)

	def insert(self, object):
		self._current_id += 1
		object['uid'] = self._current_id
		self._objects[self._current_id] = object
		self._update_indexes(object)
		return self._current_id

	def create_index(self, key, indextype):
		cls = IndexesMap[indextype]

		if key not in self._indexes:
			self._indexes[key] = cls(key=key)
			self._indexes[indextype] = self._indexes[key]
		
	def _update_indexes(self, obj):
		for key, index in self._indexes.items():
			if key in obj:
				index.add(obj)

		

		