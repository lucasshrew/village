from collections import namedtuple, Sequence

class AABB:
    def __init__(self, x, y, right, bottom):
        self.x = x
        self.y = y
        self.w = right - x
        self.h = bottom - y
        self.right = right
        self.bottom = bottom

    def contains(self, position):
        point = position
        if point.x < self.x:
            return False
        if point.x > self.x + self.w:
            return False
        if point.y < self.y:
            return False
        if point.y > self.y + self.h:
            return False

        return True

    def divide(self):
        a = AABB(self.x, self.y, self.x + self.w/2, self.y + self.h/2)
        b = AABB(self.x + self.w/2, self.y, self.right, self.y + self.h/2)
        c = AABB(self.x + self.w/2, self.y + self.h/2, self.right, self.bottom)
        d = AABB(self.x, self.y + self.h/2, self.x + self.w/2, self.bottom)

        return a,b,c,d

    def intersects(self, other):
        if self.x > other.right:
            return False
        if self.right < other.x:
            return False
        if self.y > other.bottom:
            return False
        if self.bottom < other.y:
            return False
        return True

    def __repr__(self):
        return "({}, {}) ({}, {})".format(self.x, self.y, self.right, self.bottom)


class QuadTree:
    def __init__(self, max_capacity=100, aabb=AABB(-180, -180, 180, 180), key='position'):
        self.max_capacity = max_capacity
        self.aabb = aabb
        self.data = []
        self.childs = []
        self.key = key

    def add(self, element):
        if not self.aabb.contains(Point._make(element[self.key])):
            return

        for child in self.childs:
            child.add(element)

        if len(self.childs) == 0:
            self.data.append(element)
            if len(self.data) > self.max_capacity:
                self.create_childs()

    def create_childs(self):
        self.childs = [QuadTree(self.max_capacity, aabb, key=self.key) for aabb in self.aabb.divide()]

        for p in self.data:
            self.add(p)

        self.data = []

    def _get_points(self, circle):
        points = [ p for p in self.data if circle.contains(Point._make(p[self.key])) ]
        return points

    def _get_points_in_circle(self, circle):
        if not self.aabb.intersects(circle.get_aabb()):
            return []

        if len(self.data) > 0:
            return self._get_points(circle)

        points = []
        for q in self.childs:
            points += q._get_points_in_circle(circle)

        return points

    def get_near_points(self, point, rad):
        point = Point._make(point)
        circle = Circle(point, rad)
        return self._get_points_in_circle(circle)


Point = namedtuple('Point', ['x', 'y'])

class Circle:
    def __init__(self, center, radius):
        self.x = center.x
        self.y = center.y

        self.rad = radius

    def get_aabb(self):
        return AABB(self.x-self.rad, self.y-self.rad, self.x+self.rad, self.y+self.rad)

    def contains(self, point):
        squared_dist = (self.x - point.x) ** 2 + (self.y - point.y) ** 2
        return squared_dist <= self.rad ** 2

class QElement(dict):
    def __init__(self, id, pos, msg):
        super(QElement, self).__init__(id=id, position=pos, message=msg)

    def __getattr__(self, name):
        return self[name]

    def __setattr__(self, name, value):
        self[name] = value

