
import unittest

import db
from db import memdb, mongo

class MemDBInterfaceTest(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        super(MemDBInterfaceTest, self).__init__(*args, **kwargs)
        self._db_impl = memdb

    def setUp(self):
        self._db = self._db_impl.Client()
    
    def tearDown(self):
        self._db.drop_database('db_test')        

    def test_create_db(self):
        db = self._db.connect('db_test')
        self.assertIsNotNone(db)

    def test_create_collection(self):
        self._db.connect('db_test')
        collection = self._db.get_collection('foo')
        self.assertIsNotNone(collection)
    
    def test_insert(self):
        self._db.connect('db_test')
        coll = self._db.get_collection('foo')
        id1 = coll.insert({'location': [10, 10], 'payload':'foo'})
        self.assertIsNotNone(id1)
        
        id2 = coll.insert({'location': [20, 10], 'payload':'bar'})
        self.assertNotEqual(id1, id2)

    def test_get_all(self):
        self._db.connect('db_test')
        coll = self._db.get_collection(name='foo')
        id1 = coll.insert({'location': [10, 10], 'payload':'foo'})
        id2 = coll.insert({'location': [20, 10], 'payload':'bar'})
        id3 = coll.insert({'location': [20, 20], 'payload':'baz'})

        objs = coll.get_all()
        self.assertEqual(len(list(objs)), 3)

    def test_proximity_query(self):
        self._db.connect('db_test')
        coll = self._db.get_collection(name='geo')

        coll.create_index('location', db.Indexes.GEO)

        id1 = coll.insert({'location': [10, 0], 'payload':'foo'})
        id2 = coll.insert({'location': [0, 10], 'payload':'bar'})
        id3 = coll.insert({'location': [-10, 0], 'payload':'baz'})
        id4 = coll.insert({'location': [0, -10], 'payload':'baz'})
        id5 = coll.insert({'location': [50, -50], 'payload':'baz'})

        objs = coll.get_near(center=(0,0), distance=30)
        self.assertEqual(len(list(objs)), 4)


class MongoDBInterfaceTest(MemDBInterfaceTest):
    def __init__(self, *args, **kwargs):
        super(MongoDBInterfaceTest, self).__init__(*args, **kwargs)
        self._db_impl = mongo
