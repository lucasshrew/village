#!/bin/python

import unittest
from tornado.testing import AsyncHTTPTestCase

import json
from urllib.parse import urlencode
from tornado.httpclient import HTTPRequest


import server
from db import quadtree
from db.quadtree import QuadTree, AABB, Point, QElement

class TestServerApi(AsyncHTTPTestCase):

    def get_app(self):
        return server.application

    def test_index(self):
        response = self.fetch('/')
        self.assertEqual(response.code, 200)
        self.assertIsNotNone(response.body)

    def test_add(self):
        server.quadtree = QuadTree(100, AABB(-180, -180, 180, 180))
        
        self.assertEqual(len(server.quadtree.data), 0)

        query = {'x': 10, 'y': 20, 'msg':'foo'}
        response = self.fetch('/add', method='POST', body=urlencode(query), follow_redirects=False)
        
        self.assertEqual(response.code, 200)
        self.assertIs(response.body, b'')

        self.assertEqual(len(server.quadtree.data), 1)
        self.assertEqual(server.quadtree.data[0].position, Point(10, 20))

    def test_query(self):
        url = '/query/{}/{}'.format(0, 0)
        
        response = self.fetch(url)
        self.assertEqual(response.code, 200)
        
        body = response.body.decode('utf-8')
        self.assertEqual(json.loads(body), json.loads('{"response":[]}'))

        server.quadtree.add(QElement(1, Point(10, 0), "hola"))
        server.quadtree.add(QElement(1, Point(0, 10), "hola"))

        points = server.quadtree.get_near_points(Point(0, 0), 20)
        result = [{'id': p.id, 'position': [p.position.x, p.position.y]} for p in points]
        result = dict({'response': result})

        response = self.fetch(url)
        self.assertEqual(response.code, 200)
        
        body = response.body.decode('utf-8')
        self.assertEqual(json.loads(body), result)

        self.assertIn('application/json', response.headers['Content-Type'])


if __name__ == '__main__':
    unittest.main()
