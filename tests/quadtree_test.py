import unittest
import sys
#sys.path.append(".")

from db import quadtree
from db.quadtree import QuadTree, AABB, Point, Circle, QElement

class QuadTreeTest(unittest.TestCase):
    def setUp(self):
        self.qt = QuadTree(2, AABB(-100, -100, 100, 100))
        self.elems = (QElement(1, Point(-90, -30), "bla"), QElement(2, Point(30, -40), "ble"), QElement(3, Point(30, 40), "blu"), QElement(4, Point(-30, 40), "bli"))
        for e in self.elems:
            self.qt.add(e)

    def test_split(self):
        qt = QuadTree(2, AABB(-100, -100, 100, 100))
        qt.add(QElement(1, Point(-90, -30), "hi"))
        qt.add(QElement(1, Point(30, -40), "bye"))
        self.assertEqual(2, len(qt.data))

        self.assertEqual(self.qt.childs[0].data[0], self.elems[0])
        self.assertEqual(self.qt.childs[1].data[0], self.elems[1])
        self.assertEqual(self.qt.childs[2].data[0], self.elems[2])
        self.assertEqual(self.qt.childs[3].data[0], self.elems[3])

        self.qt.add(QElement(1, Point(-31, 41), "end"))
        self.qt.add(QElement(1, Point(-32, 42), "start"))
        self.assertEqual(len(self.qt.childs[3].childs), 4)
    
    def test_search(self):
        points = self.qt.get_near_points(Point(25, 45), 20)
        self.assertEqual(len(points), 1)
        
        points = self.qt.get_near_points(Point(0, 0), 50)
        self.assertEqual(len(points), 3)

        self.assertIn(self.elems[1], points)
        self.assertIn(self.elems[2], points)
        self.assertIn(self.elems[3], points)
        self.assertNotIn(self.elems[0], points)


class AABBTest(unittest.TestCase):
    def test_contains(self):
        aabb = AABB(-10, -10, 10, 10)
        self.assertTrue(aabb.contains(Point(1, 1)))

    def test_divide(self):
        childs = AABB(-10, -10, 10, 10).divide()
        self.assertEqual(4, len(childs))
        a1,a2,a3,a4 = childs
        self.assertEqual(a1.x, -10)
        self.assertEqual(a1.y, -10)
        self.assertEqual(a1.w, 10)
        self.assertEqual(a1.h, 10)

        self.assertEqual(a2.x, 0)
        self.assertEqual(a2.y, -10)
        self.assertEqual(a2.w, 10)
        self.assertEqual(a2.h, 10)

        self.assertEqual(a3.x, 0)
        self.assertEqual(a3.y, 0)
        self.assertEqual(a3.w, 10)
        self.assertEqual(a3.h, 10)

        self.assertEqual(a4.x, -10)
        self.assertEqual(a4.y, 0)
        self.assertEqual(a4.w, 10)
        self.assertEqual(a4.h, 10)

    def test_intersects(self):
        entity_aabb = AABB(-1, -1, 1, 1)
        aabb = AABB(-10, -10, 10, 10)
        outsider = AABB(20, 20, 30, 30)
        overlap = AABB(8, 8, 15, 15)
        overlap2 = AABB(-12, -12, -9, -9)
        edge = AABB(10, 10, 19, 19)

        self.assertTrue(aabb.intersects(entity_aabb))
        self.assertFalse(aabb.intersects(outsider))
        self.assertTrue(aabb.intersects(overlap))
        self.assertTrue(aabb.intersects(overlap2))
        self.assertTrue(aabb.intersects(edge))
        

class PointTest(unittest.TestCase):
    def test_equal(self):
        p1 = Point(10, 30)
        p2 = Point(10, 30)
        p3 = p1
        self.assertEqual(p1, p2)
        self.assertNotEqual(p1, Point(40, 30))
        self.assertEqual(p1, p3)

class CircleTest(unittest.TestCase):
    def test_contains(self):
        c = Circle(Point(0,0), 10)
        self.assertTrue(c.contains(Point(0,0)))
        self.assertTrue(c.contains(Point(10,0)))
        self.assertTrue(c.contains(Point(-10,0)))
        self.assertTrue(c.contains(Point(0,10)))
        self.assertTrue(c.contains(Point(0,-10)))
        self.assertTrue(c.contains(Point(7,7)))

        self.assertFalse(c.contains(Point(10,10)))




if __name__ == '__main__':
    unittest.main()
