import tornado.ioloop
import tornado.web

from db import quadtree
from db.quadtree import QuadTree, AABB, Point, QElement

quadtree = QuadTree(100, AABB(-180, -180, 180, 180))
next_id = 1

class MainHandler(tornado.web.RequestHandler):
    def get(self):
        self.render("add.html")


class AddHandler(tornado.web.RequestHandler):
    def post(self):
        global quadtree
        global next_id
        x = float(self.get_argument("x"))
        y = float(self.get_argument("y"))
        msg = self.get_argument("msg")

        quadtree.add(QElement(next_id, Point(x, y), msg))
        next_id += 1


class QueryHandler(tornado.web.RequestHandler):
    def get(self, x, y):
        elements = quadtree.get_near_points(Point(float(x), float(y)), 20)

        response = [{'id': e.id, 'position': [e.position.x, e.position.y], 'message':e.message} for e in elements]
        self.write({'response': response})


application = tornado.web.Application([
        (r"/", MainHandler),
        (r"/add", AddHandler),
        (r"/query/([-+]?[0-9]*\.?[0-9]*)/([-+]?[0-9]*\.?[0-9]*)", QueryHandler)
], template_path='templates',
    static_path='static',
    static_url_prefix='/static/',
    debug=True
)


if __name__ == "__main__":
    application.listen(8888)
    tornado.ioloop.IOLoop.instance().start()

