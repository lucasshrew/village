from pymongo import MongoClient, GEO2D

db = None

def init_db():
    global db
    db = MongoClient().geo_example
    db.places.create_index([("loc", GEO2D)])

def populate_db():
    for x in range(-180, 180, 1):
        for y in range(-180, 180, 1):
            db.places.insert({"loc": [x, y]})

def run_query():
    docs = list(db.places.find({"loc": {"$within": {"$center": [[0, 0], 40]}}}))
    print(len(docs))
    print(docs[0])
    print(docs[-1])


def main():
    init_db()

    import sys
    if sys.argv[0] == 'init':
        populate_db()

    run_query()

if __name__=='__main__':
    main()
