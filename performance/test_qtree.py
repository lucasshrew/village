import db
from db.quadtree import QuadTree, AABB, Point, Circle, QElement

db = None

def init_db():
    global db
    db = QuadTree(100, AABB(-180, -180, 180, 180))

def populate_db():
    i = 0
    for x in range(-180, 180, 1):
        for y in range(-180, 180, 1):
            q = QElement(i, Point(x, y))
            db.add(q)
            i += 1

def run_query():
    docs = db.get_near_points(Point(0, 0), 40)
    print(len(docs))
    print(docs[0])
    print(docs[-1])


def main():
    init_db()

    populate_db()

    run_query()

if __name__=='__main__':
    main()
